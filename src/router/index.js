import { createRouter, createWebHashHistory } from "vue-router";

import AdaptorView from "../views/AdaptorView.vue";
import GameView from "../views/GameView.vue";
import EditorView from "../views/EditorView.vue";
import GameSettings from "../views/GameSettings.vue";

const routes = [
  {
    path: "/",
    name: "AdaptorView",
    component: AdaptorView,
  },
  {
    path: "/game",
    name: "GameView",
    component: GameView,
  },
  {
    path: "/settings",
    name: "GameSettings",
    component: GameSettings,
  },
  {
    path: "/editor",
    name: "EditorView",
    component: EditorView,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

function hasQueryParams(route) {
  return !!Object.keys(route.query).length;
}

router.beforeEach((to, from, next) => {
  if (!hasQueryParams(to) && hasQueryParams(from)) {
    next({ name: to.name, query: from.query });
  } else {
    next();
  }
});

export default router;
