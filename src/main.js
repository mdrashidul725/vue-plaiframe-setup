import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./styles/main.scss";
import "./styles/colors.scss";
import Icon from '@/components/Icon'

createApp(App)
  .component("Icon", Icon)
  .use(store)
  .use(router)
  .mount("#app");
