import { createStore, createLogger } from "vuex";
import { actions } from "./actions";
import { mutations } from "./mutations";
import { getters } from "./getters";

export default createStore({
  strict: true,
  state: {
    // client stuff
    user: "my name", // my name
    prompt:{
      visible:false,
      message:null,
      webhook:null,
      schema:null,
    },

    selected: {
      game: null, // name of the game e.g. "test"
      level: null, // name of level open in editor e.g. "how_to",
      action: null, // name of action selected. if not null sidebar is open, getter for schema
      state: null, // if action selected, or state name etc.
      is_live: false, // UI is in live mode
    },

    data: {
      game_list: null, // list of all games
      game: null, // list of all levels in selected game
      level: null, // object with level data
      level_schema: null, // level schema
      actions: null, // list of all actions
      sessions: null, // list of all sessions in current game
    },
  },
  actions,
  mutations,
  getters,
  plugins:
    process.env.NODE_ENV == "debug"
      ? [
          createLogger({
            collapsed: false, // auto-expand logged mutations
            // eslint-disable-next-line no-unused-vars
            filter(mutation, stateBefore, stateAfter) {
              // returns `true` if a mutation should be logged
              // `mutation` is a `{ type, payload }`
              return mutation.type !== "aBlocklistedMutation";
            },
            // eslint-disable-next-line no-unused-vars
            actionFilter(action, state) {
              // same as `filter` but for actions
              // `action` is a `{ type, payload }`
              return action.type !== "aBlocklistedAction";
            },
            transformer(state) {
              // transform the state before logging it.
              // for example return only a specific sub-tree
              return state;
            },
            mutationTransformer(mutation) {
              // mutations are logged in the format of `{ type, payload }`
              // we can format it any way we want.
              return mutation;
            },
            actionTransformer(action) {
              // Same as mutationTransformer but for actions
              return action.type;
            },
            logActions: true, // Log Actions
            logMutations: true, // Log mutations
            logger: console, // implementation of the `console` API, default `console`
          }),
        ]
      : [],
});
