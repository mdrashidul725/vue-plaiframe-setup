import { cloneDeep } from "lodash";

export const getters = {
  selectedActionData: (state) => {
    const id = state.selected.action;
    return state.data.level?.actions[id];
  },
  selectedActionSchema: (state) => {
    // TODO: refactor to new actions endpoint
    const id = state.selected.action;
    const selected = state.data.level.actions[id].action;
    const schema = state.data.actions?.find(
      (a) => a.action == selected
    )?.schema;
    return schema;
  },
  statesNameList: (state) => {
    return Object.values(state.data.level.states).reduce((accu, curr) => {
      accu.push(curr.name);
      return accu;
    }, []);
  },
  stateByName: (state) => (name) => {
    let entries = Object.entries(state.data.level.states);
    let match = entries.find((e) => e[1].name === name);
    return match;
  },
  actionsByState: (state) => (stateId) => {
    const stateData = state["data"]["level"]["states"][stateId];
    return [...stateData.listen, ...stateData.run].reduce(
      (res, key) => ((res[key] = state["data"]["level"]["actions"][key]), res),
      {}
    );
  },
  sumActionsInState: (state, getters) => (stateId, actionType) => {
    const all = Object.entries(getters.actionsByState(stateId));
    return actionType
      // eslint-disable-next-line no-unused-vars
      ? all.filter(([key, value]) =>
          value ? value.action === actionType : false
        ).length
      : Object.keys(all).length;
  },
  contentsByState: (state, getters) => (stateId) => {
    const stateActions = getters.actionsByState(stateId);
    const allContentIds = Object.keys(state["data"]["level"]["contents"]);
    let values = [];
    const getAllValues = (obj) => {
      for (let key in obj) {
        typeof obj[key] === "object"
          ? getAllValues(obj[key])
          : values.push(obj[key]);
      }
    };
    getAllValues(stateActions);
    const stateContentIds = values.filter((value) =>
      allContentIds.includes(value)
    );
    return stateContentIds.reduce(
      (res, key) => ((res[key] = state["data"]["level"]["contents"][key]), res),
      {}
    );
  },
  contentIdsByAction: (state) => (actionId) => {
    const contents = [];
    const resolveContent = (obj) => {
      for (let key in obj) {
        if (typeof obj[key] === "object") {
          resolveContent(obj[key]);
        } else if (
          typeof obj[key] === "string" &&
          obj[key].startsWith("_content")
        ) {
          contents.push(obj[key]);
        }
      }
      return obj;
    };
    resolveContent(state.data.level.actions[actionId]);
    return contents;
  },
  actionByIdResolved: (state, getters) => (actionId) => {
    const resolved = cloneDeep(state.data.level.actions[actionId]);
    const resolveContent = (obj) => {
      for (let key in obj) {
        if (typeof obj[key] === "object") {
          resolveContent(obj[key]);
        } else if (
          typeof obj[key] === "string" &&
          obj[key].startsWith("_content")
        ) {
          obj[key] = getters.contentById(obj[key]);
        }
      }
      return obj;
    };
    resolveContent(resolved);
    return resolved;
  },
  contentById: (state) => (contentId) => {
    // FIXME: multi language support
    return state.data.level.contents[contentId]["de"];
  },
  actionsSortedByPlugin: (state) => () => {
    const all = state.data.actions;
    return all?.reduce((result, currentValue) => {
      if (!result[currentValue["plugin"]]) result[currentValue["plugin"]] = [];
      result[currentValue["plugin"]].push(currentValue);
      return result;
    }, {});
  },
};
