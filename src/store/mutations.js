import { merge } from "lodash";

export const mutations = {
  setData(state, { key, value }) {
    state["data"][key] = value;
  },
  setSelected(state, { key, value }) {
    state["selected"][key] = value;
  },
  setLevelName(state, name) {
    state.data.level.name = name;
  },
  setPrompt(state, {message, webhook, schema, visible}){
    state.prompt  = {message, webhook, schema, visible};
  },
  updateState(state, { key, value }) {
    merge(state.data.level.states[key], value);
  },
  updateAction(state, { key, value }) {
    state.data.level.actions[key] = value;
  },
  addActionToState(state, { stateId, actionId, actionMode }) {
    state.data.level.states[stateId][actionMode].push(actionId);
  },
  addPluginItem(state, { plugin, collection, data }) {
    const pluignObj = state.data.plugins.find((p) => p.name == plugin);
    pluignObj.items[collection].items.push(data);
  },
  updatePluginItem(state, { plugin, collection, index, data }) {
    const pluignObj = state.data.plugins.find((p) => p.name == plugin);
    pluignObj.items[collection].items[index] = data;
  },
  deletePluginItem(state, { plugin, collection, index }) {
    const pluignObj = state.data.plugins.find((p) => p.name == plugin);
    pluignObj.items[collection].items.splice(index,1);
  },
  updatePlugin(state, { name, data }) {
    merge(
      state.data.plugins.find((p) => p.name == name),
      data
    );
  },
  addStateToStates(state, { stateId, position }) {
    // TODO: rework this to respect schema
    state.data.level.states = {
      ...state.data.level.states,
      [stateId]: {
        name: stateId,
        view: { position: { x: position.x, y: position.y } },
        run: [],
        listen: [],
      },
    };
  },
  updateContent(state, { contentId, value }) {
    // FIXME: add language support
    state.data.level.contents[contentId] = { de: value };
  },
  deleteState(state, stateId) {
    delete state.data.level.states[stateId];
  },
  deleteContent(state, { contentId }) {
    delete state.data.level.contents[contentId];
  },
  deleteActionFromState(state, { stateId, actionId }) {
    const stateRef = state.data.level.states[stateId];
    const runIndex = stateRef["run"].indexOf(actionId);
    const listenIndex = stateRef["listen"].indexOf(actionId);
    if (runIndex >= 0) {
      stateRef["run"].splice(runIndex, 1);
    } else if (listenIndex >= 0) {
      stateRef["listen"].splice(listenIndex, 1);
    }
  },
};
