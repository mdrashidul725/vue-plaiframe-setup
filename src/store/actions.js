import { RestApi } from "@/services/api";
import { nanoid } from "nanoid";
import { cloneDeep } from "lodash";

const api = new RestApi().getClient();

export const actions = {
  fetchSessions: async ({ commit }, { game, level }) => {
    let sessions = await api
      .findSessions({ game, level_name: level })
      .catch((e) => {
        if (e.response.status == 404) {
          return { data: null }; // returns 404 when no sessions available. better would be: returning empty data
        }
        console.error(
          "API",
          e.response.data.name,
          e.response.data.message,
          e.response
        );
      });
    console.log(sessions.data);
    commit("setData", { key: "sessions", value: sessions.data });
  },
  launchSession: async ({ dispatch }, { game, level, session, args }) => {
    api
      .launchSession({ game }, { level, name: session, arguments: args })
      .then((res) => {
        console.log("session launched", res);
        dispatch("fetchSessions", { game });
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  cancelSession: async ({ dispatch }, { game, session }) => {
    api
      .cancelSession({ game, session })
      .then((res) => {
        console.log("session cancelled", res);
        dispatch("fetchSessions", { game });
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  fetchAvailablePlugins: async ({ commit }) => {
    api
      .getPlugins()
      .then((res) =>
        commit("setData", { key: "available_plugins", value: res.data })
      )
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  fetchGameList: ({ commit }) => {
    api
      .getGames()
      .then((res) => commit("setData", { key: "game_list", value: res.data }))
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  fetchPlugins: ({ commit, state }) => {
    api
      .findPlugins(state.selected.game)
      .then((res) => commit("setData", { key: "plugins", value: res.data }))
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  selectGame: ({ commit }, game) => {
    commit("setSelected", { key: "game", value: game });
    api
      .getGame(game)
      .then((res) => commit("setData", { key: "game", value: res.data }))
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  selectLevel: async ({ commit, state, dispatch }, { gameId, levelId }) => {
    const game = gameId;
    const [level, actions, level_schema] = await Promise.all([
      api.getLevel({ game, level: levelId }).then((res) => res.data),
      api.getActions(game).then((res) => res.data),
      api.getLevelSchema(game).then((res) => res.data),
    ]).catch((e) =>
      console.error("API", e.response.data.name, e.response.data.message)
    );

    commit("setData", { key: "level", value: level });
    commit("setData", { key: "actions", value: actions });
    commit("setData", { key: "level_schema", value: level_schema });

    commit("setSelected", { key: "level", value: levelId });
    commit("setSelected", { key: "game", value: game });
    if (!state.data.game) dispatch("selectGame", game);
  },
  selectAction: ({ state, commit }, { actionId, stateId }) => {
    if (state["selected"]["action"] != actionId)
      commit("setSelected", { key: "action", value: actionId });
    if (state["selected"]["state"] != stateId)
      commit("setSelected", { key: "state", value: stateId });
  },
  toggleSelectAction: ({ state, commit }, { actionId, stateId }) => {
    if (state["selected"]["action"] != actionId)
      commit("setSelected", { key: "action", value: actionId });
    else commit("setSelected", { key: "action", value: null });
    if (state["selected"]["state"] != stateId)
      commit("setSelected", { key: "state", value: stateId });
  },
  selectState: ({ state, commit }, stateId) => {
    if (state["selected"]["state"] != stateId) {
      commit("setSelected", { key: "action", value: null });
      commit("setSelected", { key: "state", value: stateId });
    }
  },
  unselectAll: ({ state, commit }) => {
    if (state["selected"]["action"] != null)
      commit("setSelected", { key: "action", value: null });
    if (state["selected"]["state"] != null)
      commit("setSelected", { key: "state", value: null });
  },
  //////////////////////////////////////////////////////////////////////////////
  // ACTIONS ON LEVEL DATA
  //////////////////////////////////////////////////////////////////////////////
  renameLevel: ({ commit, dispatch }, name) => {
    commit("setLevelName", name);
    dispatch("syncLevel", name);
  },
  addActionToState: (
    { state, getters, dispatch, commit },
    { stateId, actionName }
  ) => {
    // TODO getter based on new actions list format
    const actionSchema = state["data"]["actions"].find(
      (a) => a.action === actionName
    );
    const actionId = actionSchema.action + "_" + nanoid(10);

    commit("addActionToState", {
      stateId,
      actionId,
      actionMode: actionSchema.mode,
    });
    // TODO render empty dataset based on schema
    commit("updateAction", {
      key: actionId,
      value: {
        action: actionSchema.action,
        plugin: actionSchema.plugin,
        mode: actionSchema.mode,
        name: `${actionSchema.action}_${
          getters.sumActionsInState(stateId, actionSchema.action) + 1
        }`,
        payload: {},
      },
    });
    dispatch("syncState", { stateId });
  },
  addState: ({ commit, dispatch }, { actionName, position }) => {
    const stateId = "_state_" + nanoid(10);
    commit("addStateToStates", { stateId, position });
    // TODO: wait for state to init
    dispatch("addActionToState", { stateId, actionName }).then(() =>
      dispatch("syncState", { stateId })
    );
  },
  updateState: ({ commit }, { key, value }) => {
    commit("updateState", { key, value });
  },
  updateAction: (
    { getters, commit, dispatch },
    { actionId, value, stateId }
  ) => {
    // TODO: only update if something changed
    // Note: to reuse existing action content ids we create a pool that gets used before new ones are created
    const existingIds = getters.contentIdsByAction(actionId);
    // eslint-disable-next-line
    const contentKeys = [
      "text",
      "respond",
      "caption",
      "contains",
      "regex",
      "equals",
      "value",
      "play",
      "say",
      "flow",
      "file",
    ];

    const deepIterate = (obj) => {
      for (let key in obj) {
        if (typeof obj[key] === "object") {
          deepIterate(obj[key]);
        }
        // if content is in array of strings
        else if (
          contentKeys.includes(key) &&
          Array.isArray(obj[key]) &&
          obj[key].every((i) => typeof i !== "object")
        ) {
          for (const index in obj[key]) {
            const contentId = existingIds.shift() || "_content_" + nanoid(10);
            const contentValue = obj[key][index];
            obj[key][index] = contentId;
            commit("updateContent", { contentId, value: contentValue });
          }
        }
        // if content is in single strings
        else if (contentKeys.includes(key) && typeof obj[key] !== "object") {
          const contentId = existingIds.shift() || "_content_" + nanoid(10);
          const contentValue = obj[key];
          obj[key] = contentId;
          commit("updateContent", { contentId, value: contentValue });
        }
      }
      return obj;
    };
    value = deepIterate(cloneDeep(value)); // BUG: we need to clone this, but WHY?!?

    commit("updateAction", { key: actionId, value });
    if (existingIds.length >= 1)
      dispatch("deleteContents", { contentIds: existingIds }); // delete remaining contentIds
    dispatch("syncState", { stateId });
  },
  syncState: ({ state, getters }, { stateId }) => {
    const game = state["selected"]["game"];
    const level = state["data"]["level"]["_id"];
    const payload = {
      states: { [stateId]: state["data"]["level"]["states"][stateId] },
      actions: getters.actionsByState(stateId),
      contents: getters.contentsByState(stateId), // TODO: this seems very expensive to do every time!!!
    };
    api
      .createState({ game, level }, payload)
      .then((res) => console.log("API called successfully.", res))
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  deleteState({ state, commit }, stateId) {
    const game = state["selected"]["game"];
    const level = state["data"]["level"]["_id"];
    console.log("deleting state", stateId);
    api
      .deleteState({ game, level, state: stateId })
      .then(() => {
        console.log("API called successfully.");
        commit("deleteState", stateId);
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  deleteAction({ dispatch, getters, commit }, { actionId, stateId }) {
    dispatch("deleteContents", {
      contentIds: getters.contentIdsByAction(actionId),
    });
    commit("deleteActionFromState", { stateId, actionId });
    dispatch("syncState", { stateId });
  },
  deleteContents({ commit }, { contentIds }) {
    contentIds.forEach((contentId) => {
      commit("deleteContent", { contentId });
    });
  },
  addPlugin({ state, dispatch }, { game, plugin }) {
    if (!game) game = state["selected"]["game"];
    console.log("adding plugin", plugin);
    api
      .addPlugin({ game }, { name: plugin })
      .then(() => {
        dispatch("selectGame", game);
        dispatch("fetchPlugins");
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  deletePlugin({ state, dispatch }, { game, plugin }) {
    if (!game) game = state["selected"]["game"];
    console.log("deleting plugin", plugin);
    api
      .removePlugin({ game, plugin })
      .then(() => {
        dispatch("selectGame", game);
        dispatch("fetchPlugins");
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  addItemToPlugin: ({ state, commit }, { game, plugin, collection, data }) => {
    if (!game) game = state["selected"]["game"];
    if (!data) data = { name: `${collection}_${nanoid(5)}` };
    data.is_not_synced = true;
    commit("addPluginItem", { plugin, collection, data });
  },

  updatePluginItem({ commit }, { plugin, collection, index, data }) {
    commit("updatePluginItem", { plugin, collection, index, data });
  },
  updatePlugin: ({ commit }, { name, data }) => {
    commit("updatePlugin", { name, data });
  },
  syncPluginSettings({ state }, { game, plugin }) {
    if (!game) game = state["selected"]["game"];
    const data = state.data.plugins.find((p) => p.name == plugin);
    api
      .updatePluginSettings({ game, plugin }, data.settings.data)
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  reconnectPluinItem({ state, dispatch }, { game, plugin, collection, index }) {
    if (!game) game = state["selected"]["game"];
    const obj = state.data.plugins.find((p) => p.name == plugin);
    const data = obj.items[collection].items[index];
    api
      .reconnectPluginItem({
        game,
        plugin,
        plugin_collection: collection,
        item: data._id,
      })
      .then((res) => {
        if (res?.data?.message) {
          dispatch("triggerPrompt", {
            message: res.data.message,
            webhook: res.data.webhook,
            schema: res.data.schema,
          });
        } else {
          console.log("reconnectPluginItem", res);
        }
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message, e)
      );
  },
  triggerPrompt({ commit }, { message, webhook, schema }) {
    commit("setPrompt", { message, webhook, schema, visible: true });
  },
  cancelPrompt({ commit }) {
    commit("setPrompt", {
      message: null,
      webhook: null,
      schema: null,
      visible: false,
    });
  },
  async answerPrompt({ dispatch, state }, answer) {
    const response = await fetch(
      process.env.VUE_APP_API_URL + state.prompt.webhook,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(answer),
      }
    ).catch((e) =>
      console.error(
        "answerPrompt",
        e.response.data.name,
        e.response.data.message
      )
    );
    const content = await response.json();

    console.log("answerPrompt got", content);
    dispatch("cancelPrompt");
  },
  syncPluginItem({ state, commit }, { game, plugin, collection, index }) {
    if (!game) game = state["selected"]["game"];
    const obj = state.data.plugins.find((p) => p.name == plugin);
    const data = obj.items[collection].items[index];
    if (data.is_not_synced) {
      const dataTemp = cloneDeep(data);
      delete dataTemp.is_not_synced;
      return api
        .createPluginItem(
          { game, plugin, plugin_collection: collection },
          dataTemp
        )
        .then((res) => {
          console.log(res);
          return commit("updatePluginItem", {
            plugin,
            collection,
            data: Object.assign(dataTemp, { _id: res.data.created_id }),
            index: index,
          });
        })
        .catch((e) =>
          console.error("API", e.response.data.name, e.response.data.message)
        );
    } else {
      return api
        .editPluginItem(
          { game, plugin, plugin_collection: collection, item: data._id },
          data
        )
        .catch((e) =>
          console.error("API", e.response.data.name, e.response.data.message)
        );
    }
  },
  deletePluginItem: (
    { state, commit },
    { game, plugin, collection, index }
  ) => {
    if (!game) game = state["selected"]["game"];
    const pluginTemp = state.data.plugins.find((p) => p.name == plugin);
    const item = pluginTemp.items[collection].items[index]["name"];
    console.log({ game, plugin, plugin_collection: collection, item: item });
    api
      .deletePluginItem({
        game,
        plugin,
        plugin_collection: collection,
        item: item,
      })
      .then(() => {
        commit("deletePluginItem", { plugin, collection, index });
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  //////////////////////////////////////////////////////////////////////////////
  // ACTIONS ON GAME DATA
  //////////////////////////////////////////////////////////////////////////////
  createGame: ({ dispatch }, config) => {
    api.createGame(null, { name: config.name, template: "multiplayer" }).then(
      (res) => {
        console.log("API called successfully.", res.data);
        dispatch("fetchGameList");
      },
      (error) => console.error(error)
    );
  },
  deleteGame: ({ dispatch }, config) => {
    api
      .deleteGame({ game: config.name })
      .then(() => {
        console.log("API called successfully.");
        dispatch("fetchGameList");
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  addLevel: ({ state, dispatch }, config) => {
    console.log(config);
    api
      .createLevel(
        { game: config.game },
        {
          name: config.name,
          config: {
            arguments: { Player: {} },
          },
        }
      )
      .then(() => {
        console.log("API called successfully.");
        dispatch("selectGame", state.data.game.name);
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  deleteLevel: ({ state, dispatch }, config) => {
    console.log("action deleteLevel got", config);
    api
      .deleteLevel({ game: config.game, level: config.level })
      .then(() => {
        console.log("API called successfully.");
        dispatch("selectGame", state.data.game.name);
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
  syncLevel: ({ state }, name) => {
    console.log("Sync ", name);
    const game = state["selected"]["game"];
    const level = state["data"]["level"]["_id"];
    const payload = state["data"]["level"];
    api
      .updateLevel({ game, level }, payload)
      .then(() => {
        console.log("API called successfully.");
      })
      .catch((e) =>
        console.error("API", e.response.data.name, e.response.data.message)
      );
  },
};
