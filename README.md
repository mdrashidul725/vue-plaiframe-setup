# vue-plaiframe-setup

## Project setup

```console
npm install
```

after this: [setup configuration](#Customize-configuration) if not using defaults.

### Compiles and hot-reloads for development

```console
npm run serve
```

### Compiles and minifies for production

```console
npm run build
```

### Lints and fixes files

```console
npm run lint
```

### Customize configuration

edit `.env.debug`, `.env.development` and/or `.env.production` to modify `VUE_APP_API_URL` and other environment variables relevant to your build, if you dont want to use the default value.

For vue specific configuration see [Configuration Reference](https://cli.vuejs.org/config/).

### Useful Links

_Vue Enterprise Boilerplate_ for Vuex Store and Component Architecture, also VS Code Settings
[Vue Enterprise Boilerplate](https://github.com/bencodezen/vue-enterprise-boilerplate)

## Generate Api Client from openapi.yaml

### How The Rest Api Was Build

The Rest Api located in `src/service/api.js` is based on [openapi-client-axios](https://github.com/anttiviljami/openapi-client-axios). [Documentation](https://github.com/anttiviljami/openapi-client-axios/blob/master/DOCS.md#authentication)
